package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Users;

@RestController
@RequestMapping("/mainapp")
public class ResourceController {

	@Autowired
	private EmployeeDao ed;
	
	@GetMapping("/lu")
	public Users loadUsers() {

		return ed.getAllUsers();
	}

}
