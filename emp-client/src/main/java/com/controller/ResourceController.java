package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
public class ResourceController {

	@Autowired
	private AppDelegate appDelegate;
	
	@GetMapping("/loadall")
	public String loadUsers() {

		return appDelegate.loadUSers();
	}

}
