package com.controller;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AppDelegate {

	
	public String loadUSers() {
		RestTemplate restTemplate=new RestTemplate();
		return restTemplate.exchange("http://localhost:8090/mainapp/lu"
				,HttpMethod.GET
				,null
				,new ParameterizedTypeReference<String>() {
				}).getBody();
		
	}
	
}
